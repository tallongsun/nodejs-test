var async = require("async");

var domains = ["a","b","c","d","e"]
var maps = {"key1":"val1","key2":"val2"}
var nullMaps = {}
async.forEach(nullMaps, function (domain,notice) {
    var d = domain.trim();
    xyz(d,function (result) {
        notice(null,result)
    })
},function (err,res) {
    if(err){
        console.log("[error]"+err)
        return
    }
    console.log(res)
})

function xyz(domain, callback){
    console.log("[xyz]"+domain)
    callback(domain)
}
