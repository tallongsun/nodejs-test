var request = require('request');
let xml =
`<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header/>
  <soap:Body>
    <getWeather xmlns="http://WebXml.com.cn/">
      <theCityCode>902</theCityCode>
      <theUserID></theUserID>
    </getWeather>
  </soap:Body>
</soap:Envelope>`
console.log(xml.length)

var options = {
    url: 'http://ws.webxml.com.cn/WebServices/WeatherWS.asmx?wsdl',
    method: 'POST',
    body: xml,
    headers: {
        'Content-Type':'text/xml',
        'User-Agent': 'curl/7.64.0'
    }
};

let callback = (error, response, body) => {
    if (!error && response.statusCode == 200) {
        console.log('Raw result', body);
        var xml2js = require('xml2js');
        var parser = new xml2js.Parser({explicitArray: false, trim: true});
        parser.parseString(body, (err, result) => {
            console.log('JSON result', result);
        });
    };
    console.log('E', response.statusCode, response.statusMessage);
};
request(options, callback);

