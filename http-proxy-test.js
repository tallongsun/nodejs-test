var express = require('express');
var proxy = require('http-proxy-middleware');

var exampleProxy = proxy('/proxy/api',{
  target: 'http://www.example.org', // target host
  changeOrigin: true, // needed for virtual hosted sites
  ws: true, // proxy websockets
  pathRewrite: function(path,req){
    var p = path.replace('/proxy/api','/api')
    console.log('path rewrite ' + p)
    return p
  },
  onProxyRes: function(proxyRes,req,res){
    console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2));
    console.log('Request', JSON.stringify(req.headers, true, 2));
  },
  logLevel: 'debug',
  router: {
    // when request.headers.host == 'dev.localhost:3000',
    // override target 'http://www.example.org' to 'http://localhost:8000'
    'dev.localhost:3000': 'http://localhost:8000'
  }
});

var app=express();
app.use('/proxy/api', exampleProxy);
app.listen(3000);
